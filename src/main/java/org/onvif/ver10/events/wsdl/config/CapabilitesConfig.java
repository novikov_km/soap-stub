package org.onvif.ver10.events.wsdl.config;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.onvif.ver10.events.wsdl.EventPortTypeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

@Configuration
public class CapabilitesConfig {

    @Value("${server.endpoint}")
    private String addr;

    @Autowired
    private Bus bus;

    @Bean(name = "endpoint")
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, new EventPortTypeImpl());
        endpoint.publish(addr);
        return endpoint;
    }
}
